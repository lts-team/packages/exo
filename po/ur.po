# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Muhammad Ali Makki <makki.ma@gmai.com>, 2009
msgid ""
msgstr ""
"Project-Id-Version: Exo\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-07-02 22:30+0200\n"
"PO-Revision-Date: 2013-11-19 13:01+0000\n"
"Last-Translator: Nick <nick@xfce.org>\n"
"Language-Team: Urdu (http://www.transifex.com/projects/p/xfce/language/ur/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ur\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../exo/exo-cell-renderer-ellipsized-text.c:131
#: ../exo/exo-cell-renderer-icon.c:144
msgid "Follow state"
msgstr "متبع حالت"

#: ../exo/exo-cell-renderer-ellipsized-text.c:132
#: ../exo/exo-cell-renderer-icon.c:145
msgid "Render differently based on the selection state."
msgstr "انتخابی حالت کو مد نظر رکھتے ہوئے مختلف طور پر رینڈر کریں"

#: ../exo/exo-cell-renderer-icon.c:166
msgid "Icon"
msgstr "آئکن"

#: ../exo/exo-cell-renderer-icon.c:167
msgid "The icon to render."
msgstr "آئکن برائے رینڈر."

#: ../exo/exo-cell-renderer-icon.c:185
msgid "GIcon"
msgstr ""

#: ../exo/exo-cell-renderer-icon.c:186
msgid "The GIcon to render."
msgstr ""

#: ../exo/exo-cell-renderer-icon.c:207
msgid "size"
msgstr "حجم"

#: ../exo/exo-cell-renderer-icon.c:208
msgid "The size of the icon to render in pixels."
msgstr "پکسل میں رینڈر کے لیے آئکن کا حجم."

#: ../exo/exo-gdk-pixbuf-extensions.c:787
#, c-format
msgid "Failed to open file \"%s\": %s"
msgstr "فائل کھولنے میں ناکامی \"%s\": %s"

#: ../exo/exo-gdk-pixbuf-extensions.c:849
#, c-format
msgid "Failed to read file \"%s\": %s"
msgstr "فائل پڑھنے میں ناکامی \"%s\": %s"

#: ../exo/exo-gdk-pixbuf-extensions.c:890
#, c-format
msgid "Failed to load image \"%s\": Unknown reason, probably a corrupt image file"
msgstr "تصویر \"%s\" لوڈ نہیں ہوسکی: نامعلوم وجہ، تصویر فائل خراب ہوسکتی ہے"

#: ../exo/exo-gtk-extensions.c:227
#, c-format
msgid "Failed to open \"%s\"."
msgstr " \"%s\" کھولنے میں ناکامی"

#: ../exo/exo-icon-bar.c:277 ../exo/exo-icon-view.c:806
msgid "Orientation"
msgstr "جہتیابی"

#: ../exo/exo-icon-bar.c:278
msgid "The orientation of the iconbar"
msgstr "آئکن پٹی کی جہتیابی"

#: ../exo/exo-icon-bar.c:294 ../exo/exo-icon-view.c:823
msgid "Pixbuf column"
msgstr "Pixbuf کالم"

#: ../exo/exo-icon-bar.c:295 ../exo/exo-icon-view.c:824
msgid "Model column used to retrieve the icon pixbuf from"
msgstr "نمونہ کالم pixbuf آئکن کو حاصل کرنے کے لیے استعمال ہوتا ہے از"

#: ../exo/exo-icon-bar.c:310 ../exo/exo-icon-view.c:950
msgid "Text column"
msgstr "متن کالم"

#: ../exo/exo-icon-bar.c:311 ../exo/exo-icon-view.c:951
msgid "Model column used to retrieve the text from"
msgstr "نمونہ کالم متن حاصل کرنے کے لیے استعمال ہوتا ہے از"

#: ../exo/exo-icon-bar.c:323
msgid "Icon Bar Model"
msgstr "آئکن پٹی ماڈل"

#: ../exo/exo-icon-bar.c:324
msgid "Model for the icon bar"
msgstr "آئکن پٹی کے لیے ماڈل"

#: ../exo/exo-icon-bar.c:340
msgid "Active"
msgstr "فعال"

#: ../exo/exo-icon-bar.c:341
msgid "Active item index"
msgstr "فعال عنصر فہرست"

#: ../exo/exo-icon-bar.c:347 ../exo/exo-icon-bar.c:348
msgid "Active item fill color"
msgstr "فعال عنصر کا بھرائی رنگ"

#: ../exo/exo-icon-bar.c:354 ../exo/exo-icon-bar.c:355
msgid "Active item border color"
msgstr "فعال عنصر کا بارڈر رنگ"

#: ../exo/exo-icon-bar.c:361 ../exo/exo-icon-bar.c:362
msgid "Active item text color"
msgstr "فعال عنصر کا متن رنگ"

#: ../exo/exo-icon-bar.c:368 ../exo/exo-icon-bar.c:369
msgid "Cursor item fill color"
msgstr "کرسر عنصر کا بھرائی رنگ"

#: ../exo/exo-icon-bar.c:375 ../exo/exo-icon-bar.c:376
msgid "Cursor item border color"
msgstr "کرسر عنصر کا بارڈر رنگ"

#: ../exo/exo-icon-bar.c:382 ../exo/exo-icon-bar.c:383
msgid "Cursor item text color"
msgstr "کرسر عنصر کا متن رنگ"

#. EXO_ICON_CHOOSER_CONTEXT_ACTIONS
#: ../exo/exo-icon-chooser-dialog.c:109
msgid "Action Icons"
msgstr "آئکن حرکت"

#. EXO_ICON_CHOOSER_CONTEXT_ANIMATIONS
#: ../exo/exo-icon-chooser-dialog.c:111
msgid "Animations"
msgstr "متحرکات"

#. EXO_ICON_CHOOSER_CONTEXT_APPLICATIONS
#: ../exo/exo-icon-chooser-dialog.c:113
msgid "Application Icons"
msgstr "اطلاقیہ آئکن"

#. EXO_ICON_CHOOSER_CONTEXT_CATEGORIES
#: ../exo/exo-icon-chooser-dialog.c:115
msgid "Menu Icons"
msgstr "مینیو آئکن"

#. EXO_ICON_CHOOSER_CONTEXT_DEVICES
#: ../exo/exo-icon-chooser-dialog.c:117
msgid "Device Icons"
msgstr "ڈوائس آئکن"

#. EXO_ICON_CHOOSER_CONTEXT_EMBLEMS
#: ../exo/exo-icon-chooser-dialog.c:119
msgid "Emblems"
msgstr "علامات"

#. EXO_ICON_CHOOSER_CONTEXT_EMOTES
#: ../exo/exo-icon-chooser-dialog.c:121
msgid "Emoticons"
msgstr "اموٹیکنز"

#. EXO_ICON_CHOOSER_CONTEXT_INTERNATIONAL
#: ../exo/exo-icon-chooser-dialog.c:123
msgid "International Denominations"
msgstr "عالمی شناخت"

#. EXO_ICON_CHOOSER_CONTEXT_MIME_TYPES
#: ../exo/exo-icon-chooser-dialog.c:125
msgid "File Type Icons"
msgstr "فائل قسم آئکن"

#. EXO_ICON_CHOOSER_CONTEXT_PLACES
#: ../exo/exo-icon-chooser-dialog.c:127
msgid "Location Icons"
msgstr "مقام آئکن"

#. EXO_ICON_CHOOSER_CONTEXT_STATUS
#: ../exo/exo-icon-chooser-dialog.c:129
msgid "Status Icons"
msgstr "حالت آئکن"

#. EXO_ICON_CHOOSER_CONTEXT_OTHER
#: ../exo/exo-icon-chooser-dialog.c:131
msgid "Uncategorized Icons"
msgstr "بے زمرہ آئکن"

#. EXO_ICON_CHOOSER_CONTEXT_ALL
#: ../exo/exo-icon-chooser-dialog.c:135
msgid "All Icons"
msgstr "تمام آئکن"

#. EXO_ICON_CHOOSER_CONTEXT_FILE
#: ../exo/exo-icon-chooser-dialog.c:139 ../exo/exo-icon-chooser-dialog.c:285
msgid "Image Files"
msgstr "تصویر فائل"

#. setup the context combo box
#: ../exo/exo-icon-chooser-dialog.c:210
msgid "Select _icon from:"
msgstr "م_نتخب آئکن از:"

#. search filter
#: ../exo/exo-icon-chooser-dialog.c:225
msgid "_Search icon:"
msgstr ""

#: ../exo/exo-icon-chooser-dialog.c:236
msgid "Clear search field"
msgstr ""

#: ../exo/exo-icon-view.c:674
msgid "Column Spacing"
msgstr "کالم خلا"

#: ../exo/exo-icon-view.c:675
msgid "Space which is inserted between grid column"
msgstr "خلا جو grid کالم کے درمیان ڈالا جائے گا"

#: ../exo/exo-icon-view.c:691
msgid "Number of columns"
msgstr "کالموں کی تعداد"

#: ../exo/exo-icon-view.c:692
msgid "Number of columns to display"
msgstr "ظاہر ہونے والے کالموں کی تعداد"

#: ../exo/exo-icon-view.c:706
msgid "Enable Search"
msgstr "تلاش فعال کریں"

#: ../exo/exo-icon-view.c:707
msgid "View allows user to search through columns interactively"
msgstr "منظر صارف کو کالم کے درمیان فعال طور پر تلاش کی صلاحیت دیتا ہے"

#: ../exo/exo-icon-view.c:724
msgid "Width for each item"
msgstr "ہر عنصر کی چوڑائی"

#: ../exo/exo-icon-view.c:725
msgid "The width used for each item"
msgstr "چوڑائی جو ہر عنصر کے لیے استعمال ہوگی"

#: ../exo/exo-icon-view.c:743
msgid "Layout mode"
msgstr "خاکہ طرز"

#: ../exo/exo-icon-view.c:744
msgid "The layout mode"
msgstr "خاکہ طرز"

#: ../exo/exo-icon-view.c:760
msgid "Margin"
msgstr "مارجن"

#: ../exo/exo-icon-view.c:761
msgid "Space which is inserted at the edges of the icon view"
msgstr "خلا جو آئکن منظر کے کناروں پر ڈالا جائے گا"

#: ../exo/exo-icon-view.c:777
msgid "Markup column"
msgstr "کالم نشان زدہ کریں"

#: ../exo/exo-icon-view.c:778
msgid "Model column used to retrieve the text if using Pango markup"
msgstr "اگر Pango markup استعمال کیا جائے تو نمونہ کالم متن کے حصول کے لیے استعمال کیا جاتا ہے"

#: ../exo/exo-icon-view.c:792
msgid "Icon View Model"
msgstr "آئکن منظر ماڈل"

#: ../exo/exo-icon-view.c:793
msgid "The model for the icon view"
msgstr "آئکن منظر کے لیے ماڈل"

#: ../exo/exo-icon-view.c:807
msgid ""
"How the text and icon of each item are positioned relative to each other"
msgstr "کس طرح ہر عنصر کی آئکن اور متن ایک دوسرے سے متعلق جگہ لیں گے"

#: ../exo/exo-icon-view.c:839
msgid "Reorderable"
msgstr "پھر قابلِ ترتیب"

#: ../exo/exo-icon-view.c:840
msgid "View is reorderable"
msgstr "منظر پھر قابلِ ترتیب ہے"

#: ../exo/exo-icon-view.c:855
msgid "Row Spacing"
msgstr "سطر خلا"

#: ../exo/exo-icon-view.c:856
msgid "Space which is inserted between grid rows"
msgstr "خلا جو grid سطور کے درمیان ڈالا جائے گا"

#: ../exo/exo-icon-view.c:870
msgid "Search Column"
msgstr "تلاش کالم"

#: ../exo/exo-icon-view.c:871
msgid "Model column to search through when searching through item"
msgstr "عنصر میں سے تلاشنے کے لیے نمونہ کا کالم"

#: ../exo/exo-icon-view.c:885
msgid "Selection mode"
msgstr "انتخاب طرز"

#: ../exo/exo-icon-view.c:886
msgid "The selection mode"
msgstr "انتخاب طرز"

#: ../exo/exo-icon-view.c:901 ../exo/exo-tree-view.c:154
msgid "Single Click"
msgstr "ایک کلک"

#: ../exo/exo-icon-view.c:902 ../exo/exo-tree-view.c:155
msgid "Whether the items in the view can be activated with single clicks"
msgstr "ایک کلک طرز میں عناصر کس طرح فعال ہوں گے"

#: ../exo/exo-icon-view.c:918 ../exo/exo-tree-view.c:171
msgid "Single Click Timeout"
msgstr "ایک کلک کا انتہائی وقت"

#: ../exo/exo-icon-view.c:919 ../exo/exo-tree-view.c:172
msgid ""
"The amount of time after which the item under the mouse cursor will be "
"selected automatically in single click mode"
msgstr "ایک کلک طرز کا وہ انتہائی وقت جس میں کسی عنصر پر ماؤس کرسر لے جانے پر وہ خود بخود منتخب ہوجائے گا"

#: ../exo/exo-icon-view.c:934
msgid "Spacing"
msgstr "خلا"

#: ../exo/exo-icon-view.c:935
msgid "Space which is inserted between cells of an item"
msgstr "خلا جو عنصر کے خلیے کے درمیان ڈالا جائے گا"

#: ../exo/exo-thumbnail-preview.c:113
msgid "Preview"
msgstr "معائنہ"

#: ../exo/exo-thumbnail-preview.c:129 ../exo/exo-thumbnail-preview.c:271
msgid "No file selected"
msgstr "کوئی فائل منتخب نہیں"

#: ../exo/exo-thumbnail-preview.c:289
msgid "Block Device"
msgstr "بلاک ڈیوائس"

#: ../exo/exo-thumbnail-preview.c:294
msgid "Character Device"
msgstr "رموز ڈیوائس"

#: ../exo/exo-thumbnail-preview.c:299
msgid "Folder"
msgstr "فولڈر"

#: ../exo/exo-thumbnail-preview.c:304
msgid "FIFO"
msgstr "FIFO"

#: ../exo/exo-thumbnail-preview.c:309
msgid "Socket"
msgstr "ساکٹ"

#: ../exo/exo-toolbars-editor-dialog.c:118
msgid "_Add a new toolbar"
msgstr "_نئی اوزار پٹی شامل کریں"

#: ../exo/exo-toolbars-editor.c:221
msgid ""
"Drag an item onto the toolbars above to add it, from the toolbars in the "
"items table to remove it."
msgstr "عنصر کو شامل کرنے کے لیے اوپر اوزار کی پٹی میں اسے ڈریگ کرکے چھوڑیں، ختم کرنے کے لیے اوزار کی پٹی سے ڈریگ کرکے عناصر ٹیبل میں چھوڑیں."

#: ../exo/exo-toolbars-editor.c:537
msgid "Separator"
msgstr "جداگار"

#: ../exo/exo-toolbars-view.c:752
msgid "Toolbar _Style"
msgstr "اوزار پٹی کا ان_داز"

#: ../exo/exo-toolbars-view.c:759
msgid "_Desktop Default"
msgstr "_ڈیسک ٹاپ طے شدہ"

#: ../exo/exo-toolbars-view.c:768
msgid "_Icons only"
msgstr "صرف آئکن"

#: ../exo/exo-toolbars-view.c:777
msgid "_Text only"
msgstr "صرف متن"

#: ../exo/exo-toolbars-view.c:786
msgid "Text for _All Icons"
msgstr "متن برائے ت_مام آئکن"

#: ../exo/exo-toolbars-view.c:795
msgid "Text for I_mportant Icons"
msgstr "خاص آئکن کا متن"

#: ../exo/exo-toolbars-view.c:803
msgid "_Remove Toolbar"
msgstr "اوزار پ_ٹی حذف کریں"

#: ../exo/exo-toolbars-view.c:818
msgid "Customize Toolbar..."
msgstr "اوزار پٹی مختص کریں..."

#: ../exo/exo-wrap-table.c:148
msgid "Column spacing"
msgstr "کالم خلا"

#: ../exo/exo-wrap-table.c:149
msgid "The amount of space between two consecutive columns"
msgstr "دو متواتر کالم کے درمیان خلا کی مقدار"

#: ../exo/exo-wrap-table.c:163
msgid "Row spacing"
msgstr "سطر خلا"

#: ../exo/exo-wrap-table.c:164
msgid "The amount of space between two consecutive rows"
msgstr "دو متواتر سطور کے درمیان خلا کی مقدرا"

#: ../exo/exo-wrap-table.c:178
msgid "Homogeneous"
msgstr "ہومو جینئس"

#: ../exo/exo-wrap-table.c:179
msgid "Whether the children should be all the same size"
msgstr "کب تمام چھوٹوں کو ایک حجم کا ہونا چاہیے"

#: ../exo/exo-xsession-client.c:210
msgid "Window group"
msgstr "ونڈو گروپ"

#: ../exo/exo-xsession-client.c:211
msgid "Window group leader"
msgstr "ونڈو گروپ لیڈر"

#: ../exo/exo-xsession-client.c:221
msgid "Restart command"
msgstr "ری سٹارٹ کمانڈ"

#: ../exo/exo-xsession-client.c:222
msgid "Session restart command"
msgstr "نشست ری سٹارٹ کمانڈ"

#: ../exo-csource/main.c:284
#, c-format
msgid "Usage: %s [options] [file]\n"
msgstr "استعمال: %s [اختیار] [فائل]\n"

#: ../exo-csource/main.c:285
#, c-format
msgid "       %s [options] --build-list [[name file]...]\n"
msgstr "       %s [اختیارات] --بلڈ-لسٹ [[فائل کا نام]...]\n"

#: ../exo-csource/main.c:287
#, c-format
msgid "  -h, --help        Print this help message and exit\n"
msgstr "  -h, --ہدایت        ہدایت پیغام طبع کرکے برخاست کریں\n"

#: ../exo-csource/main.c:288
#, c-format
msgid "  -V, --version     Print version information and exit\n"
msgstr "  -V, --ورژن    ورژن کی معلومات پرنٹ کرکے اخراج کریں\n"

#: ../exo-csource/main.c:289
#, c-format
msgid "  --extern          Generate extern symbols\n"
msgstr "  --خارجی          خارجی رموز جنریٹ کریں\n"

#: ../exo-csource/main.c:290
#, c-format
msgid "  --static          Generate static symbols\n"
msgstr "  --ساکن          ساکن رموز جنریٹ کریں\n"

#: ../exo-csource/main.c:291
#, c-format
msgid "  --name=identifier C macro/variable name\n"
msgstr "  --name=identifier C ماکرو/ویریئبل کا نام\n"

#: ../exo-csource/main.c:292
#, c-format
msgid "  --build-list      Parse (name, file) pairs\n"
msgstr "  --build-list      حرفی تجزی (نام, فائل) حرفی تجزیہ\n"

#: ../exo-csource/main.c:293
#, c-format
msgid "  --strip-comments  Remove comments from XML files\n"
msgstr "  --strip-comments  تبصرے XML فائلوں سے حذف کرتے ہیں\n"

#: ../exo-csource/main.c:294
#, c-format
msgid "  --strip-content   Remove node contents from XML files\n"
msgstr "  --strip-content   XML فائلوں سے node مواد حذف کرتے ہیں\n"

#: ../exo-csource/main.c:304 ../exo-desktop-item-edit/main.c:195
#: ../exo-open/main.c:496
#, c-format
msgid ""
"Copyright (c) %s\n"
"        os-cillation e.K. All rights reserved.\n"
"\n"
"Written by Benedikt Meurer <benny@xfce.org>.\n"
"\n"
msgstr "کاپی رائٹ (c) %s\n        os-cillation e.K. تمام حقوق محفوظ ہیں\n\nلکھا گیا از Benedikt Meurer <benny@xfce.org>.\n\n"

#: ../exo-csource/main.c:308 ../exo-desktop-item-edit/main.c:199
#: ../exo-open/main.c:500
#, c-format
msgid ""
"%s comes with ABSOLUTELY NO WARRANTY,\n"
"You may redistribute copies of %s under the terms of\n"
"the GNU Lesser General Public License which can be found in the\n"
"%s source package.\n"
"\n"
msgstr "%s مکمل طور پر بغیر کسی ضمانت کے پیش کیا جاتا ہے\nآپ %s کی کاپیاں زیرِ شرائط برائے\nGNU جنرل پبلک لائسنس جو\n%s کے مصدر پیکج موجود ہے.\n\n"

#: ../exo-csource/main.c:312 ../exo-desktop-item-edit/main.c:203
#: ../exo-open/main.c:504
#, c-format
msgid "Please report bugs to <%s>.\n"
msgstr "بگ رپورٹ کریں تا <%s>.\n"

#. allocate the file chooser
#: ../exo-desktop-item-edit/exo-die-command-entry.c:255
msgid "Select an Application"
msgstr "اطلاقیہ منتخب کریں"

#: ../exo-desktop-item-edit/exo-die-command-entry.c:264
#: ../exo-helper/exo-helper-chooser.c:398
msgid "All Files"
msgstr "تمام فائلیں"

#: ../exo-desktop-item-edit/exo-die-command-entry.c:269
#: ../exo-helper/exo-helper-chooser.c:403
msgid "Executable Files"
msgstr "اطلاقی فائلیں"

#: ../exo-desktop-item-edit/exo-die-command-entry.c:284
#: ../exo-helper/exo-helper-chooser.c:418
msgid "Perl Scripts"
msgstr "پرل سکرپٹ"

#: ../exo-desktop-item-edit/exo-die-command-entry.c:290
#: ../exo-helper/exo-helper-chooser.c:424
msgid "Python Scripts"
msgstr "پائتھن سکرپٹ"

#: ../exo-desktop-item-edit/exo-die-command-entry.c:296
#: ../exo-helper/exo-helper-chooser.c:430
msgid "Ruby Scripts"
msgstr "روبی سکرپٹ"

#: ../exo-desktop-item-edit/exo-die-command-entry.c:302
#: ../exo-helper/exo-helper-chooser.c:436
msgid "Shell Scripts"
msgstr "شیل سکرپٹ"

#: ../exo-desktop-item-edit/exo-die-desktop-model.c:288
#, c-format
msgid "Create Launcher <b>%s</b>"
msgstr "<b>%s</b> لاؤنچر بنائیں"

#. TRANSLATORS: Label in "Create Launcher"/"Create Link" dialog, make sure to
#. avoid mnemonic conflicts
#: ../exo-desktop-item-edit/exo-die-editor.c:297
msgid "_Name:"
msgstr "ن_ام:"

#. TRANSLATORS: Label in "Create Launcher"/"Create Link" dialog, make sure to
#. avoid mnemonic conflicts
#: ../exo-desktop-item-edit/exo-die-editor.c:313
msgid "C_omment:"
msgstr "تبص_رہ:"

#. TRANSLATORS: Label in "Create Launcher" dialog, make sure to avoid mnemonic
#. conflicts
#: ../exo-desktop-item-edit/exo-die-editor.c:329
msgid "Comm_and:"
msgstr "کم_انڈ:"

#. TRANSLATORS: Label in "Create Link" dialog, make sure to avoid mnemonic
#. conflicts
#: ../exo-desktop-item-edit/exo-die-editor.c:344
msgid "_URL:"
msgstr "ربط:"

#. TRANSLATORS: Label in "Create Launcher" dialog, make sure to avoid mnemonic
#. conflicts
#: ../exo-desktop-item-edit/exo-die-editor.c:360
msgid "Working _Directory:"
msgstr ""

#. TRANSLATORS: Label in "Create Launcher"/"Create Link" dialog, make sure to
#. avoid mnemonic conflicts
#: ../exo-desktop-item-edit/exo-die-editor.c:389
msgid "_Icon:"
msgstr "آئ_کن:"

#. TRANSLATORS: Label for the icon button in "Create Launcher"/"Create Link"
#. dialog if no icon selected
#. setup a label to tell that no icon was selected
#: ../exo-desktop-item-edit/exo-die-editor.c:406
#: ../exo-desktop-item-edit/exo-die-editor.c:1233
msgid "No icon"
msgstr "بلا آئکن"

#: ../exo-desktop-item-edit/exo-die-editor.c:412
msgid "Options:"
msgstr "اختیارات:"

#. TRANSLATORS: Check button label in "Create Launcher" dialog, make sure to
#. avoid mnemonic conflicts
#. *              and sync your translations with the translations in Thunar
#. and xfce4-panel.
#. 
#: ../exo-desktop-item-edit/exo-die-editor.c:421
msgid "Use _startup notification"
msgstr "آغاز اط_لاعیہ استعمال کریں"

#: ../exo-desktop-item-edit/exo-die-editor.c:422
msgid ""
"Select this option to enable startup notification when the command is run "
"from the file manager or the menu. Not every application supports startup "
"notification."
msgstr "سٹارٹ اپ انتباہ کے لیے یہ آپشن منتخب کریں جب کمانڈ فائل منیجر یا مینیو سے چلائی جائے. نوٹ کریں کہ ہر اطلاقیہ سٹارٹ اپ انتباہ کی معاونت نہیں رکھتا."

#. TRANSLATORS: Check button label in "Create Launcher" dialog, make sure to
#. avoid mnemonic conflicts
#. *              and sync your translations with the translations in Thunar
#. and xfce4-panel.
#. 
#: ../exo-desktop-item-edit/exo-die-editor.c:434
msgid "Run in _terminal"
msgstr "ٹ_رمنل میں چلائیں"

#: ../exo-desktop-item-edit/exo-die-editor.c:435
msgid "Select this option to run the command in a terminal window."
msgstr "کمانڈ ٹرمنل ونڈو میں چلانے کے لیے آپشن منتخب کریں."

#. allocate the icon chooser dialog
#: ../exo-desktop-item-edit/exo-die-editor.c:590
msgid "Select an icon"
msgstr "آئکن منتخب کریں"

#. allocate the file chooser dialog
#: ../exo-desktop-item-edit/exo-die-editor.c:637
msgid "Select a working directory"
msgstr ""

#: ../exo-desktop-item-edit/exo-die-utils.c:169
msgid "File location is not a regular file or directory"
msgstr ""

#. --- constants ---
#: ../exo-desktop-item-edit/main.c:52
msgid "Create Launcher"
msgstr "لاؤنچر بنائیں"

#: ../exo-desktop-item-edit/main.c:52
msgid "Create Link"
msgstr "ربط بنائیں"

#: ../exo-desktop-item-edit/main.c:52
msgid "Create Directory"
msgstr ""

#: ../exo-desktop-item-edit/main.c:53
msgid "Edit Launcher"
msgstr "لاؤنچر مدون کریں"

#: ../exo-desktop-item-edit/main.c:53
msgid "Edit Link"
msgstr "ربط مدون کریں"

#: ../exo-desktop-item-edit/main.c:53
msgid "Edit Directory"
msgstr ""

#: ../exo-desktop-item-edit/main.c:78
msgid "Create a new desktop file in the given directory"
msgstr "دی گئی ڈائریکٹری میں نئی ڈیسک ٹاپ فائل بنائیں"

#: ../exo-desktop-item-edit/main.c:79
msgid "Type of desktop file to create (Application or Link)"
msgstr "بنانے کے لیے ڈیسک ٹاپ فائل کی قسم (اطلاقیہ یا ربط)"

#: ../exo-desktop-item-edit/main.c:80
msgid "Preset name when creating a desktop file"
msgstr "جب ڈیسک ٹاپ فائل بنائی جائے پہلے نام سیٹ کریں"

#: ../exo-desktop-item-edit/main.c:81
msgid "Preset comment when creating a desktop file"
msgstr "جب ڈیسک ٹاپ فائل بنائی جائے پہلے کمانڈ سیٹ کریں"

#: ../exo-desktop-item-edit/main.c:82
msgid "Preset command when creating a launcher"
msgstr "جب لاؤنچر بنايا جائے پہلے کمانڈ سیٹ کریں"

#: ../exo-desktop-item-edit/main.c:83
msgid "Preset URL when creating a link"
msgstr "جب ربط بنایا جائے پہلے URL کو سیٹ کریں"

#: ../exo-desktop-item-edit/main.c:84
msgid "Preset icon when creating a desktop file"
msgstr "جب ڈیسک ٹاپ فائل بنائی جائے پہلے آئکن کو سیٹ کریں"

#: ../exo-desktop-item-edit/main.c:85 ../exo-helper/main.c:70
msgid "Print version information and exit"
msgstr "ورژن معلومات طبع کریں اور برخاست کریں"

#. initialize Gtk+
#: ../exo-desktop-item-edit/main.c:171
msgid "[FILE|FOLDER]"
msgstr "[فائل|فولڈر]"

#. no error message, the GUI initialization failed
#: ../exo-desktop-item-edit/main.c:183
msgid "Failed to open display"
msgstr "منظر کھولنے میں ناکامی"

#: ../exo-desktop-item-edit/main.c:210
msgid "No file/folder specified"
msgstr "کوئی فائل/فولڈر متعین نہیں"

#: ../exo-desktop-item-edit/main.c:276
#, c-format
msgid "Failed to load contents from \"%s\": %s"
msgstr ""

#: ../exo-desktop-item-edit/main.c:281
#, c-format
msgid "The file \"%s\" contains no data"
msgstr ""

#. failed to parse the file
#: ../exo-desktop-item-edit/main.c:294
#, c-format
msgid "Failed to parse contents of \"%s\": %s"
msgstr ""

#. we cannot continue without a type
#: ../exo-desktop-item-edit/main.c:306
#, c-format
msgid "File \"%s\" has no type key"
msgstr ""

#. tell the user that we don't support the type
#: ../exo-desktop-item-edit/main.c:316
#, c-format
msgid "Unsupported desktop file type \"%s\""
msgstr "فائل \"%s\" کی معاونت نہیں ہے"

#. add the "Create"/"Save" button (as default)
#: ../exo-desktop-item-edit/main.c:340
msgid "C_reate"
msgstr "ب_نائیں"

#. create failed, ask the user to specify a file name
#: ../exo-desktop-item-edit/main.c:513
msgid "Choose filename"
msgstr "فائل کا نام متعین کریں"

#: ../exo-desktop-item-edit/main.c:620
#, c-format
msgid "Failed to create \"%s\"."
msgstr "\"%s\" کو بنانے میں ناکامی"

#: ../exo-desktop-item-edit/main.c:620
#, c-format
msgid "Failed to save \"%s\"."
msgstr "\"%s\" کو محفوظ کرنے میں ناکامی"

#: ../exo-helper/exo-helper-chooser-dialog.c:102
#: ../exo-helper/exo-preferred-applications.desktop.in.h:1
msgid "Preferred Applications"
msgstr "مجوزہ اطلاقیے"

#: ../exo-helper/exo-helper-chooser-dialog.c:103
msgid "Select default applications for various services"
msgstr "مختلف خدمات کے لیے طے شدہ اطلاقیے منتخب کریں"

#. Internet
#. 
#: ../exo-helper/exo-helper-chooser-dialog.c:132
msgid "_Internet"
msgstr ""

#: ../exo-helper/exo-helper-chooser-dialog.c:145
#: ../exo-open/exo-web-browser.desktop.in.h:1
msgid "Web Browser"
msgstr "ویب براؤزر"

#: ../exo-helper/exo-helper-chooser-dialog.c:153
msgid ""
"The preferred Web Browser will be used to open\n"
"hyperlinks and display help contents."
msgstr "مجوزہ ویب براؤزر روابط اور ہدایات کا مواد\nکھولنے اور دیکھنے کے لیے استعمال ہوگا."

#: ../exo-helper/exo-helper-chooser-dialog.c:177
#: ../exo-open/exo-mail-reader.desktop.in.h:1
msgid "Mail Reader"
msgstr "میل ریڈر"

#: ../exo-helper/exo-helper-chooser-dialog.c:185
msgid ""
"The preferred Mail Reader will be used to compose\n"
"emails when you click on email addresses."
msgstr "ای میل روابط پر کلک کرنے پر ای میل لکھنے کے لیے\nمجوزہ ای میل پروگرام استعمال کیا جائے گا."

#. Utilities
#. 
#: ../exo-helper/exo-helper-chooser-dialog.c:205
msgid "_Utilities"
msgstr ""

#: ../exo-helper/exo-helper-chooser-dialog.c:218
#: ../exo-open/exo-file-manager.desktop.in.h:1
msgid "File Manager"
msgstr ""

#: ../exo-helper/exo-helper-chooser-dialog.c:226
msgid ""
"The preferred File Manager will be used to\n"
"browse the contents of folders."
msgstr ""

#: ../exo-helper/exo-helper-chooser-dialog.c:250
#: ../exo-open/exo-terminal-emulator.desktop.in.h:1
msgid "Terminal Emulator"
msgstr "ٹرمنل ایمولیٹر"

#: ../exo-helper/exo-helper-chooser-dialog.c:258
msgid ""
"The preferred Terminal Emulator will be used to\n"
"run commands that require a CLI environment."
msgstr "مجوزہ ٹرمنل ایمولیٹر کو ان کمانڈز کو چلانے کے لیے\nاستعمال کیا جائے گا جنہیں CLI ماحول کی ضرورت ہے."

#: ../exo-helper/exo-helper-chooser.c:149
#: ../exo-helper/exo-helper-chooser.c:156
msgid "Press left mouse button to change the selected application."
msgstr "منتخب کردہ اطلاقیوں کو بدلنے کے لیے ماؤس کا بایاں بٹن دبائیں."

#: ../exo-helper/exo-helper-chooser.c:155
msgid "Application Chooser Button"
msgstr "اطلاقیہ منتخب کار بٹن"

#: ../exo-helper/exo-helper-chooser.c:299
msgid "No application selected"
msgstr "کوئی اطلاقیہ منتخب نہیں کیا گیا"

#: ../exo-helper/exo-helper-chooser.c:315
msgid "Failed to set default Web Browser"
msgstr "طے شدہ براؤزر متعین کرنے میں ناکامی"

#: ../exo-helper/exo-helper-chooser.c:316
msgid "Failed to set default Mail Reader"
msgstr "طے شدہ میل ریڈر متعین کرنے میں ناکامی"

#: ../exo-helper/exo-helper-chooser.c:317
msgid "Failed to set default File Manager"
msgstr ""

#: ../exo-helper/exo-helper-chooser.c:318
msgid "Failed to set default Terminal Emulator"
msgstr "طے شدہ ٹرمنل ایمولیٹر متعین کرنے میں ناکامی"

#. allocate the chooser
#: ../exo-helper/exo-helper-chooser.c:388
msgid "Select application"
msgstr "اطلاقیہ منتخب کریں"

#: ../exo-helper/exo-helper-chooser.c:501
msgid "Choose a custom Web Browser"
msgstr "مخصوص ویب براؤزر منتخب کریں"

#: ../exo-helper/exo-helper-chooser.c:502
msgid "Choose a custom Mail Reader"
msgstr "مخصوص میل ریڈر منتخب کریں"

#: ../exo-helper/exo-helper-chooser.c:503
msgid "Choose a custom File Manager"
msgstr ""

#: ../exo-helper/exo-helper-chooser.c:504
msgid "Choose a custom Terminal Emulator"
msgstr "مخصوص ٹرمنل  ایمولیٹر منتخب کریں"

#: ../exo-helper/exo-helper-chooser.c:509
msgid ""
"Specify the application you want to use\n"
"as default Web Browser for Xfce:"
msgstr "چلانے کے لیے اطلاقیہ متعین کریں\nبطور Xfce طے شدہ براؤزر:"

#: ../exo-helper/exo-helper-chooser.c:510
msgid ""
"Specify the application you want to use\n"
"as default Mail Reader for Xfce:"
msgstr "استعمال کے لیے اطلاقیہ منتخب کریں\nبطور طے شدہ ایکسفس میل ریڈر:"

#: ../exo-helper/exo-helper-chooser.c:511
msgid ""
"Specify the application you want to use\n"
"as default File Manager for Xfce:"
msgstr ""

#: ../exo-helper/exo-helper-chooser.c:512
msgid ""
"Specify the application you want to use\n"
"as default Terminal Emulator for Xfce:"
msgstr "ایکسفس کے لیے طے شدہ\nٹرمنل ایمولیٹر منتخب کریں:"

#: ../exo-helper/exo-helper-chooser.c:579
msgid "Browse the file system to choose a custom command."
msgstr "مختص کمانڈ کے انتخاب کے لیے فائل سسٹم براؤز کریں."

#: ../exo-helper/exo-helper-chooser.c:762
msgid "_Other..."
msgstr "_دیگر..."

#: ../exo-helper/exo-helper-chooser.c:763
msgid "Use a custom application which is not included in the above list."
msgstr "کوئی مختص اطلاقیہ استعمال کریں جو اوپر فہرست میں نہیں ہے."

#: ../exo-helper/exo-helper-launcher-dialog.c:113
msgid "Choose Preferred Application"
msgstr "مجوزہ اطلاقیے منتخب کریں"

#: ../exo-helper/exo-helper-launcher-dialog.c:256
msgid ""
"Please choose your preferred Web\n"
"Browser now and click OK to proceed."
msgstr "اپنا مجوزہ ویب براؤزر منتخب کریں\nاور آگے بڑھنے کے لیے ٹھیک ہے پر کلک کریں."

#: ../exo-helper/exo-helper-launcher-dialog.c:258
msgid ""
"Please choose your preferred Mail Reader\n"
"now and click OK to proceed."
msgstr "اپنا مجوزہ ای میل ریڈر منتخب کریں\nاور آگے بڑھیں."

#: ../exo-helper/exo-helper-launcher-dialog.c:260
msgid ""
"Please choose your preferred File Manager\n"
"now and click OK to proceed."
msgstr ""

#: ../exo-helper/exo-helper-launcher-dialog.c:262
msgid ""
"Please choose your preferred Terminal\n"
"Emulator now and click OK to proceed."
msgstr "اپنا مجوزہ ٹرمنل ایمولیٹر منتخب\nکریں اور ٹھیک ہے سے آگے بڑھیں"

#: ../exo-helper/exo-helper.c:382
#, c-format
msgid "No command specified"
msgstr "کوئی کمانڈ متعین نہیں کی گئی"

#: ../exo-helper/exo-helper.c:688 ../exo-helper/exo-helper.c:722
#, c-format
msgid "Failed to open %s for writing"
msgstr "لکھنے کے لیے %s کو کھولنے میں ناکامی"

#: ../exo-helper/exo-preferred-applications.desktop.in.h:2
msgid ""
"Preferred Applications (Web Browser, Mail Reader and Terminal Emulator)"
msgstr "مجوزہ اطلاقیے (ویب براؤزر، ای میل ریڈر، اور ٹرمنل ایمولیٹر)"

#: ../exo-helper/main.c:41
msgid "Failed to execute default Web Browser"
msgstr "طے شدہ ویب براؤسر چلانے میں ناکامی"

#: ../exo-helper/main.c:42
msgid "Failed to execute default Mail Reader"
msgstr "طے شدہ ڈاک مطالعہ گاہ چلانے میں ناکامی"

#: ../exo-helper/main.c:43
msgid "Failed to execute default File Manager"
msgstr ""

#: ../exo-helper/main.c:44
msgid "Failed to execute default Terminal Emulator"
msgstr "طے شدہ ٹرمنل ایمولیٹر متعین کرنے میں ناکامی"

#: ../exo-helper/main.c:71
msgid ""
"Open the Preferred Applications\n"
"configuration dialog"
msgstr "ترجیح شدہ اطلاقیوں کو تشکیل دینے کا مکالمہ کھولیں"

#: ../exo-helper/main.c:72
msgid "Settings manager socket"
msgstr "ترتیبات منیجر ساکٹ"

#: ../exo-helper/main.c:72
msgid "SOCKET ID"
msgstr "ساکٹ آئی ڈی"

#: ../exo-helper/main.c:73
msgid ""
"Launch the default helper of TYPE with the optional PARAMETER, where TYPE is"
" one of the following values."
msgstr "نوعیت TYPE کا طے شدہ معاون کار بمع اختیاری PARAMETER کے ساتھ چلائیں، جہاں TYPE ذیل کی قدروں میں سے ایک قدر ہے"

#: ../exo-helper/main.c:73
msgid "TYPE [PARAMETER]"
msgstr "TYPE [PARAMETER]"

#: ../exo-helper/main.c:102
msgid ""
"The following TYPEs are supported for the --launch command:\n"
"\n"
"  WebBrowser       - The preferred Web Browser.\n"
"  MailReader       - The preferred Mail Reader.\n"
"  FileManager      - The preferred File Manager.\n"
"  TerminalEmulator - The preferred Terminal Emulator."
msgstr ""

#: ../exo-helper/main.c:112
#, c-format
msgid "Type '%s --help' for usage."
msgstr "استعمال کے لئے ٹائپ کریں '%s --help'."

#: ../exo-helper/main.c:165
#, c-format
msgid "Invalid helper type \"%s\""
msgstr "غیرموزوں معاون کار نوعیت \"%s\""

#: ../exo-helper/main.c:213
#, c-format
msgid ""
"%s (Xfce %s)\n"
"\n"
"Copyright (c) 2003-2006\n"
"        os-cillation e.K. All rights reserved.\n"
"\n"
"Written by Benedikt Meurer <benny@xfce.org>.\n"
"\n"
"Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.\n"
"\n"
"Please report bugs to <%s>.\n"
msgstr "%s (ایکسفس %s)\n\nکاپی رائٹ (c) 2003-2006\n        os-cillation e.K. تمام حقوق محفوظ ہیں.\n\nتحریر کردہ از Benedikt Meurer <benny@xfce.org>.\n\nتیار کردہ بمع Gtk+-%d.%d.%d, چل رہا ہے Gtk+-%d.%d.%d.\n\nبراہ مہربانی اغلاط کی اطلاع <%s> کو دیں.\n"

#: ../exo-helper/helpers/aterm.desktop.in.in.h:1
msgid "aterm"
msgstr "اٹرم"

#: ../exo-helper/helpers/balsa.desktop.in.in.h:1
msgid "Balsa"
msgstr "بالسا"

#: ../exo-helper/helpers/caja.desktop.in.in.h:1
msgid "Caja File Manager"
msgstr ""

#: ../exo-helper/helpers/chromium.desktop.in.in.h:1
msgid "Chromium"
msgstr ""

#: ../exo-helper/helpers/debian-sensible-browser.desktop.in.in.h:1
msgid "Debian Sensible Browser"
msgstr "ڈیبین سینسبل براؤزر"

#: ../exo-helper/helpers/debian-x-terminal-emulator.desktop.in.in.h:1
msgid "Debian X Terminal Emulator"
msgstr "ڈبین X ٹرمنل ایمولیٹر"

#: ../exo-helper/helpers/dillo.desktop.in.in.h:1
msgid "Dillo"
msgstr ""

#: ../exo-helper/helpers/encompass.desktop.in.in.h:1
msgid "Encompass"
msgstr "اینکمپاس"

#: ../exo-helper/helpers/epiphany.desktop.in.in.h:1
msgid "Epiphany Web Browser"
msgstr "اپپفنی ویب براؤزر"

#: ../exo-helper/helpers/eterm.desktop.in.in.h:1
msgid "Enlightened Terminal Emulator"
msgstr "انلائیٹینڈ ٹرمنل ایمولیٹر"

#: ../exo-helper/helpers/evolution.desktop.in.in.h:1
msgid "Evolution"
msgstr "نوول ایولوشن"

#: ../exo-helper/helpers/firefox.desktop.in.in.h:1
msgid "Mozilla Firefox"
msgstr "موزیلا فائر فاکس"

#: ../exo-helper/helpers/galeon.desktop.in.in.h:1
msgid "Galeon Web Browser"
msgstr "گیلیئن ویب براؤزر"

#: ../exo-helper/helpers/gnome-terminal.desktop.in.in.h:1
msgid "GNOME Terminal"
msgstr "گنوم ٹرمنل"

#: ../exo-helper/helpers/icecat.desktop.in.in.h:1
msgid "Icecat"
msgstr ""

#: ../exo-helper/helpers/icedove.desktop.in.in.h:1
msgid "Icedove"
msgstr ""

#: ../exo-helper/helpers/iceweasel.desktop.in.in.h:1
msgid "Iceweasel"
msgstr ""

#: ../exo-helper/helpers/jumanji.desktop.in.in.h:1
msgid "Jumanji"
msgstr ""

#: ../exo-helper/helpers/kmail.desktop.in.in.h:1
msgid "KMail"
msgstr "کے میل"

#: ../exo-helper/helpers/konqueror.desktop.in.in.h:1
msgid "Konqueror Web Browser"
msgstr "کنکرر ویب براؤزر"

#: ../exo-helper/helpers/links.desktop.in.in.h:1
msgid "Links Text Browser"
msgstr "روابط متن براؤزر"

#: ../exo-helper/helpers/lynx.desktop.in.in.h:1
msgid "Lynx Text Browser"
msgstr "Lynx متن براؤزر"

#: ../exo-helper/helpers/midori.desktop.in.in.h:1
msgid "Midori"
msgstr "میڈوری"

#: ../exo-helper/helpers/mozilla-browser.desktop.in.in.h:1
msgid "Mozilla Browser"
msgstr "موزیلا براؤزر"

#: ../exo-helper/helpers/mozilla-mailer.desktop.in.in.h:1
msgid "Mozilla Mail"
msgstr "موزیلا میل"

#: ../exo-helper/helpers/mutt.desktop.in.in.h:1
msgid "Mutt"
msgstr "مُٹ"

#: ../exo-helper/helpers/nautilus.desktop.in.in.h:1
msgid "Nautilus"
msgstr ""

#: ../exo-helper/helpers/netscape-navigator.desktop.in.in.h:1
msgid "Netscape Navigator"
msgstr "نیٹ سکیپ نیویگیٹر"

#: ../exo-helper/helpers/nxterm.desktop.in.in.h:1
msgid "NXterm"
msgstr "نیکسٹرم"

#: ../exo-helper/helpers/opera-browser.desktop.in.in.h:1
msgid "Opera Browser"
msgstr "اوپرا براؤزر"

#: ../exo-helper/helpers/opera-mailer.desktop.in.in.h:1
msgid "Opera Mail"
msgstr "اوپرا میل"

#: ../exo-helper/helpers/pcmanfm.desktop.in.in.h:1
msgid "PCMan File Manager"
msgstr ""

#: ../exo-helper/helpers/rodent.desktop.in.in.h:1
msgid "Rodent File Manager"
msgstr ""

#: ../exo-helper/helpers/rox-filer.desktop.in.in.h:1
msgid "ROX-Filer"
msgstr ""

#: ../exo-helper/helpers/sakura.desktop.in.in.h:1
msgid "Sakura"
msgstr ""

#: ../exo-helper/helpers/sylpheed.desktop.in.in.h:1
msgid "Sylpheed"
msgstr "سیلفیڈ"

#: ../exo-helper/helpers/sylpheed-claws.desktop.in.in.h:1
msgid "Claws Mail"
msgstr "کلاؤز میل"

#: ../exo-helper/helpers/Thunar.desktop.in.in.h:1
msgid "Thunar"
msgstr ""

#: ../exo-helper/helpers/thunderbird.desktop.in.in.h:1
msgid "Mozilla Thunderbird"
msgstr "موزیلا تھنڈربرڈ"

#: ../exo-helper/helpers/urxvt.desktop.in.in.h:1
msgid "RXVT Unicode"
msgstr "RXVT یونیکوڈ"

#: ../exo-helper/helpers/w3m.desktop.in.in.h:1
msgid "W3M Text Browser"
msgstr "W3M متن براؤزر"

#: ../exo-helper/helpers/xfce4-terminal.desktop.in.in.h:1
msgid "Xfce Terminal"
msgstr "اکسفس ٹرمنل"

#: ../exo-helper/helpers/xfe.desktop.in.in.h:1
msgid "Xfe File Manager"
msgstr ""

#: ../exo-helper/helpers/xterm.desktop.in.in.h:1
msgid "X Terminal"
msgstr "ایکس ٹرمنل"

#: ../exo-open/main.c:109
msgid "Usage: exo-open [URLs...]"
msgstr "استعمال: exo-open [URLs...]"

#: ../exo-open/main.c:110
msgid "       exo-open --launch TYPE [PARAMETERs...]"
msgstr "       exo-کھولیں --لاؤنچ TYPE [PARAMETERs...]"

#: ../exo-open/main.c:112
msgid "  -?, --help                          Print this help message and exit"
msgstr "  -?, --ہدایات                          ہدایت پیغام طبع کرکے برخاست کریں"

#: ../exo-open/main.c:113
msgid ""
"  -V, --version                       Print version information and exit"
msgstr "  -v, --ورژن                       ورژن معلومات طبع کرکے برخاست کریں"

#: ../exo-open/main.c:115
msgid ""
"  --launch TYPE [PARAMETERs...]       Launch the preferred application of\n"
"                                      TYPE with the optional PARAMETERs, where\n"
"                                      TYPE is one of the following values."
msgstr "  --launch TYPE [PARAMETERs...]       مجوزہ اطلاقیہ چلائیں برائے\n                                      TYPE with the optional PARAMETERs, where\n                                      TYPE is one of the following values."

#: ../exo-open/main.c:119
msgid ""
"  --working-directory DIRECTORY       Default working directory for applications\n"
"                                      when using the --launch option."
msgstr "  --working-directory DIRECTORY       اطلاقیوں کے لیے طے شدہ ورکنگ ڈائریکٹری\n                                      جب استعمال کیا جائے --لاؤنچ آپشن."

#: ../exo-open/main.c:122
msgid "The following TYPEs are supported for the --launch command:"
msgstr "مندرجہ ذیل اقسام TYPEs معاونت رکھتے ہیں --لاؤنچ کمانڈ کے لیے:"

#. Note to Translators: Do not translate the TYPEs (WebBrowser, MailReader,
#. TerminalEmulator),
#. * since the exo-helper utility will not accept localized TYPEs.
#. 
#: ../exo-open/main.c:128
msgid ""
"  WebBrowser       - The preferred Web Browser.\n"
"  MailReader       - The preferred Mail Reader.\n"
"  FileManager      - The preferred File Manager.\n"
"  TerminalEmulator - The preferred Terminal Emulator."
msgstr ""

#: ../exo-open/main.c:133
msgid ""
"If you don't specify the --launch option, exo-open will open all specified\n"
"URLs with their preferred URL handlers. Else, if you specify the --launch\n"
"option, you can select which preferred application you want to run, and\n"
"pass additional parameters to the application (i.e. for TerminalEmulator\n"
"you can pass the command line that should be run in the terminal)."
msgstr "اگر آپ نے --launch option متعین نہیں کیا تو exo-open تمام روابط کو ان\nان کے مجوزہ روابط ہینڈلر میں کھول دے گا ما سوائے اگر آپ نے لاؤنچ آپشن\nمتعین کیا تو آپ اپنے مجوزہ اطلاقیوں کو چلانے کے لیے منتخب کرسکتے ہیں\nاور اطلاقیہ کے اضافی پرامیٹرز کو پاس کر سکتے ہیں جیسے (i.e. برائے TerminalEmulator\nآپ کمانڈ لائن کو پاس کرسکتے ہیں جنہیں ٹرمنل میں چلنا چاہیے.)"

#: ../exo-open/main.c:195
#, c-format
msgid ""
"Launching desktop files is not supported when %s is compiled without GIO-"
"Unix features."
msgstr ""

#: ../exo-open/main.c:260
#, c-format
msgid "Failed to launch preferred application for category \"%s\"."
msgstr "زمرہ \"%s\" کے متعلقہ اطلاقیہ کو چلانے میں ناکامی."

#: ../exo-open/main.c:577
#, c-format
msgid "Unable to detect the URI-scheme of \"%s\"."
msgstr ""

#: ../exo-open/main.c:591
#, c-format
msgid "Failed to open URI \"%s\"."
msgstr ""

#: ../exo-open/exo-file-manager.desktop.in.h:2
msgid "Browse the file system"
msgstr ""

#: ../exo-open/exo-mail-reader.desktop.in.h:2
msgid "Read your email"
msgstr ""

#: ../exo-open/exo-terminal-emulator.desktop.in.h:2
msgid "Use the command line"
msgstr ""

#: ../exo-open/exo-web-browser.desktop.in.h:2
msgid "Browse the web"
msgstr ""
